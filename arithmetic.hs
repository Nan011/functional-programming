data Exp = C Float | Exp :- Exp | Exp :+ Exp | Exp :* Exp | Exp :/ Exp | V String | Let String Exp Exp


eval :: Exp -> Float
eval (C e) = e 
eval (e1 :- e2) = (eval e1) - (eval e2)
eval (e1 :+ e2) = (eval e1) + (eval e2)
eval (e1 :* e2) = (eval e1) * (eval e2)
-- eval (e1 :/ 0) = Nothing
eval (e1 :/ e2) = (eval e1) / (eval e2)
eval (Let v e1 e2) = eval (sub v e1 e2)


sub :: String -> Exp -> Exp -> Exp
sub v1 e1 (V v2) = if (v1 == v2) then e1 else (V v1)
sub _ _ (C c) = (C c)
sub v1 e1 (e2 :- e3) = sub v1 e1 e2 :- sub v1 e1 e3
sub v1 e1 (e2 :+ e3) = sub v1 e1 e2 :+ sub v1 e1 e3
sub v1 e1 (e2 :* e3) = sub v1 e1 e2 :* sub v1 e1 e3
sub v1 e1 (e2 :/ e3) = sub v1 e1 e2 :/ sub v1 e1 e3
sub v1 e1 (Let v2 e2 e3) = sub v1 e1 (sub v2 e2 e3)

