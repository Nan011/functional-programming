data Tree a = Node a | Branch (Tree a) a (Tree a)
treeSize :: Tree a -> Integer
treeSize (Node a) = 1
treeSize (Branch l m r) = (treeSize l) + (treeSize r) + 1


