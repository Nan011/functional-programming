primes = sieve [2..]
    where
        sieve (head:tail) = head : (sieve [e | e <- tail, e `mod` head /= 0])


pythas = [(x, y, z) | z <- [5..], y <- [4..z], x <- [3..y], z^2 == x^2 + y^2]