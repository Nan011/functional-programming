maxx :: Num a, Ord a => a -> a -> a -> a
maxx a b c 
    | a < c && b < c = c
    | a < b && c < b = b  
    | otherwise = a

