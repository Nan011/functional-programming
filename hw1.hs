
-- import qualified Hedgehog.Gen as Gen
-- import qualified Hedgehog.Range as Range
import Test.QuickCheck.Gen

-- 1 (*). Exercises from chapter 9-10 of The Craft of Functional Programming

-- 1. 
mlength :: [a] -> Integer
mlength [] = 0
mlength (head:tail) = sum (map (\item -> 1) (head:tail))

-- 2. do the right expression first and then the result will be mapped again with with same function
-- Basically what the expression do just add +1 for every element inside xs list and then add +1 more
-- So it equivalent to [e0, e1, e2, ..., en] -> [e0 + 2, e1 + 2, e2 + 2, ..., en + 2]
addTwo xs = map (+1) (map (+1) xs)
-- [GENERALIZATION]
-- map is just give you new list that every element from list be manipulated by the given function
-- and the result of (map g xs) will be mapped again by f as function 

-- Data type of iter
iter :: Integer -> (a -> a) -> a -> a 

iter 0 f a = a
iter 1 f a = f a
iter n f a = iter (n - 1) f (f a)

-- something :: Integer -> Integer -> Integer -> Integer 
something a = \n -> iter n succ

-- 3. define the sum of the squares of the natural numbers 1 to n using map and foldr
-- sqrtsum n = foldr (\e acc -> elem + acc) 0 (map (\e -> e^2) [1..n])


-- Given a list as input (xs), and then [map sing xs] will split every element from the list, 
-- wrap every element as list, and wrap those list into list again. Ex: [1, 2, 3] -> [[1], [2], [3]],
-- finally [foldr (++)] will concatenate [] with all elements of [map sing xs] by right-handed fold, so the result is same as the input
-- Ex: [[1], [2], [3]] ->  ([1] + ([2] + ([3] + []))) = [1, 2, 3]
mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

f x 
    | x > 0 = True
    | otherwise = False


-- (id . f) vs (f . id) vs (id f)

foo1 = id . f
foo2 = f . id
foo3 = id f

-- ensure the input and output must be same type, because the output will be used again as input 
-- in intuitive way we know those could be any type either input or output, for instance: (String -> Integer -> [Integer] -> String)
-- but, I think list in haskell doesn't allow that. 
-- composeList :: [a -> a] -> a

-- if empty list then return function id
composeList [] = id 
composeList (head:tail) = (composeList tail) . head
-- ex: composeList [(\x -> x + 3), (\x -> x + 1), (\x -> x - 2)] equivalent to (\x -> x - 2) . (\x -> x + 1) . (\x -> x + 3) 

mflip :: (a -> b -> c) -> (b -> a -> c)
mflip (f) x y = (f) y x

-- 2 (*). List Comprehensions and Higher-Order Functions
-- 2.a.1 
q2a1 xs = map (\e -> e + 1) xs

-- 2.a.2
q2a2 xs ys = concat (map (\x -> map (\y -> x + y) ys) xs) 

-- 2.a.3
q2a3 xs = map (\e -> e + 2) (filter (>3) xs)      

-- 2.a.4
q2a4 xyz = map (\(x, _) -> x + 3) xyz

-- 2.a.5
q2a5 xyz = map (\(x, _) -> x) (filter (\(x, y) -> x + y < 5 ) xyz)

-- 2.a.6
q2a6 xyz = map (\(Just x) -> x + 5) xyz

-- 2.b.1
q2b1 xs = [x + 3 | x <- xs]

-- 2.b.2
q2b2 xs = [x | x <- xs, x > 7]

-- 2.b.3
q2b3 xs ys = [(x, y) | x <- xs, y <- ys]

-- 2.b.4
q2b4 xys = [x + y | (x, y) <- xys, x + y > 3]


-- 3 (*). Generating Lists
-- 3.a
-- listOfLength :: Integer -> Gen a -> [Gen a]
-- listOfLength 1 g = []
-- listOfLength n g = (g n):listOfLength (n-1) g

-- Testing
-- prop_listOf n g = 

